alert('Hello Word');

console.log('Hello Word');
console.error('This is an error');
console.warn('This is a warn');

// var, let, const
// let, const

let ages = 30;
ages = 31;
console.log(ages);

const score = 10;
console.log(score);

// string, number, Boolean, null, undefined
const name = 'John';
const age = 30;
const rating = 4.5;
const isCool = true;
const x = null;
const y = undefined;
let z;

console.log(typeof name, typeof age, typeof rating, typeof isCool, typeof x, typeof y, typeof z);

// Concatenation
console.log('My name is ' + name + ' and I am ' + age);
// Template String
const hello = `My name is ${name} and I am ${age}`;
console.log(hello);

const s = 'Hello World!';
console.log(s.length);
console.log(s.toUpperCase());
console.log(s.toLowerCase());
console.log(s.substring(0, 5).toUpperCase());
console.log(s.split(''));
const q = 'technology, computer, it, code';
console.log(q.split(', '));

// Arrays - variable that hold multiple value

const number = new Array(1,2,3,4,5);
console.log(number);
const fruits = ['apples', 'oranges', 'pears'];
fruits[3] = 'grapes';
fruits.push('mangos');
fruits.unshift('stawberries');
fruits.pop();
console.log(Array.isArray(hello));
console.log(fruits.indexOf('oranges'));
console.log(fruits);

// Object Literals
const person = {
    firstName: 'John',
    lastName: 'Doe',
    age: 30,
    hobbies: ['music', 'movie', 'sport'],
    address:{
        street: '50 main st',
        city: 'Boston',
        state: 'MA'
    }
}
console.log(person.firstName, person.lastName);
console.log(person.hobbies[1]);
console.log(person.address.city);

const {firstName, lastName, address: {city}} = person;
console.log(firstName);
console.log(city);

person.email = 'john@gmail.com';
console.log(person);

const todos = [
    {
        id: 1,
        text: 'Take out trash',
        isCompleted: true
    },
    {
        id: 2,
        text: 'Meeting with boss',
        isCompleted: true
    },
    {
        id: 3,
        text: 'Dentist appt',
        isCompleted: false
    },
];

console.log(todos[1].text);
const todoJson = JSON.stringify(todos);
console.log(todoJson);

// For
for(let i = 0; i <= 10; i++){
    console.log(`For Loop Number: ${i}`);
}
for(let i = 0; i < todos.length; i++){
    console.log(todos[i].text);
}
for(let todo of todos){
    console.log(todo.id);
}

// While
let i = 0;
while(i < 10){
    console.log(`While Loop Number: ${i}`);
    i++;
}
// forEach, map, filter
todos.forEach(function(todo) {
    console.log(todo.text);
});
const todoText = todos.map(function(todo) {
    return todo.text;
});
console.log(todoText);
const todoComplated = todos.filter(function(todo) {
    return todo.isCompleted === true;
}).map(function(todo) {
    return todo.text;
})
console.log(todoComplated);

// condition
const X = 6;
const Y = 11;

if(X > 5 && Y > 10){
    console.log('X is more than 5 or Y is more than 10');
}

const XX = 9;
const color = 'Green';
switch (color) {
    case 'Red':
        console.log('color is Red');
        break;
    case 'Blue':
        console.log('color is Blue');
        break;
    default:
        console.log(' color is NOT Red or Blue');
        break;
}

//function
const addNums = num1 => num1 + 5;
console.log(addNums(5));

// OOP
//Contructor function
function Person(firstName, lastName, dob) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.dob = new Date(dob);
    this.getBirthYear = function(){
        return this.dob.getFullYear();
    }
    this.getFullName = function() {
        return `${this.firstName} ${this.lastName}`;
    }
}

Person.prototype.getBirthYear = function(){
    return this.dob.getFullYear();
}

Person.prototype.getFullName = function(){
    return `${this.firstName} ${this.lastName}`;
}

// Class
class Personn {
    constructor(firstName, lastName, dob) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = new Date(dob);
    }

    getBirthYear() {
        return this.dob.getFullYear();
    }

    getFullName() {
        return `${this.firstName} ${this.lastName}`;
    }
}

//Instantiate object
const person1 = new Person('John', 'Doe', '4-3-1980');
const person2 = new Person('Mary', 'Smith', '3-6-1970');
const person3 = new Personn('Mary', 'Smith', '3-6-1970');
console.log(person2.getFullName());
console.log(person1);
console.log(person3);

// DOM
// Single Selectors
console.log(document.getElementById('my-form'));
console.log(document.querySelector('.container'));

// MultiplE Selectors
console.log(document.querySelectorAll('.item'));
console.log(document.getElementsByTagName('li'));
console.log(document.getElementsByClassName('item'));

const items = document.querySelectorAll('.item');
items.forEach((item) => console.log(item));

// DOM Manipulation
const ul = document.querySelector('.items');
// ul.remove();
// ul.lastElementChild.remove();
ul.firstElementChild.textContent = 'Hello';
ul.children[1].innerText = 'Brad';
ul.lastElementChild.innerHTML = '<h1>Hello</h1>';

const btn = document.querySelector('.btn');

// EVENTS

// Mouse Event
btn.addEventListener('click', e => {
    e.preventDefault();
    console.log(e.target.className);
    document.getElementById('my-form').style.background = '#ccc';
    document.querySelector('body').classList.add('bg-dark');
    ul.lastElementChild.innerHTML = '<h1>Changed</h1>';
  });
  
  // Keyboard Event
//   const nameInput = document.querySelector('#name');
//   nameInput.addEventListener('input', e => {
//     document.querySelector('.container').append(nameInput.value);
//   });

  // USER FORM SCRIPT

// Put DOM elements into variables
const myForm = document.querySelector('#my-form');
const nameInput = document.querySelector('#name');
const emailInput = document.querySelector('#email');
const msg = document.querySelector('.msg');
const userList = document.querySelector('#users');

// Listen for form submit
myForm.addEventListener('submit', onSubmit);

function onSubmit(e) {
  e.preventDefault();
  
  if(nameInput.value === '' || emailInput.value === '') {
    // alert('Please enter all fields');
    msg.classList.add('error');
    msg.innerHTML = 'Please enter all fields';

    // Remove error after 3 seconds
    setTimeout(() => msg.remove(), 3000);
  } else {
    // Create new list item with user
    const li = document.createElement('li');

    // Add text node with input values
    li.appendChild(document.createTextNode(`${nameInput.value}: ${emailInput.value}`));

    // Add HTML
    // li.innerHTML = `<strong>${nameInput.value}</strong>e: ${emailInput.value}`;

    // Append to ul
    userList.appendChild(li);

    // Clear fields
    nameInput.value = '';
    emailInput.value = '';
  }
}